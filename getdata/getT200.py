# -*- coding: utf-8 -*-
"""
Created on Mon May 20 12:17:29 2019

@author: luthf
"""

import configparser
import serial
import sys
import os
from tools import datainsert
import datetime
import logging

levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

Port = config.get("T200","Port")
Baudrate = config.getint("T200","Baudrate")
Timeout = config.getint("T200","Timeout")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=levels[loglevel])

# Inisialisasi MQTT
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))
    
try:
    logging.info('Trying to use mqtt')
    
    mqhost = config.get("MQTT","hostname")
    mqport = config.getint("MQTT","port")
    mquser = config.get("MQTT","username")
    mqpass = config.get("MQTT","password")
    mqtopic = config.get("MQTT","topic")
    mqloc  = config.get("Database","username")
    
    import paho.mqtt.client as mqtt
    import json
    
    client = mqtt.Client()
    client.username_pw_set(mquser, mqpass)
    client.on_connect = on_connect
    client.connect(mqhost, mqport, 60)
    client.loop_start()
    
    use_mqtt = True
except:
    logging.info('Warning! Failed to use MQTT')
    use_mqtt = False
# end of mqtt

def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
def procT200(out):
    dtn = datetime.datetime.now(tz=datetime.timezone.utc)
    # Check for CONC1
    if out.find(b"NO2CNC1=") !=-1:
        datasplit = out.decode().split()
        a = iter(datasplit)
        for linesplit in a:
            if linesplit.find("NO2CNC1=") != -1:
                datadump = linesplit.split("=")
                val = float(datadump[1])
                unit = next(a)
        
        if unit.upper()=="UGM":
            val = val
        elif unit.upper()=="PPB":
            return val*1.88
        elif unit.upper()=="PPM":
            return val*1.88/float(1000)
        
        logging.debug('Inser to Database: NO2 = %f' % val )
        datainsert('NO2', dtn, val)
        
        # MQTT data send
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : val}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NO2'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
            
    elif out.find(b"NOXCNC1=") !=-1:
        datasplit = out.decode().split()
        a = iter(datasplit)
        for linesplit in a:
            if linesplit.find("NOXCNC1=") != -1:
                datadump = linesplit.split("=")
                val = float(datadump[1])
                unit = next(a)
                
        if unit.upper()=="UGM":
            val = val
        elif unit.upper()=="PPB":
            return val*1.88
        elif unit.upper()=="PPM":
            return val*1.88/float(1000)
        
        logging.debug('Inser to Database: NOX = %f' % val )
        datainsert('NOX', dtn, val)
        
        # MQTT data send
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : val}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NOX'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
        
    elif out.find(b"NOCNC1=") !=-1:
        datasplit = out.decode().split()
        a = iter(datasplit)
        for linesplit in a:
            if linesplit.find("NOCNC1=") != -1:
                datadump = linesplit.split("=")
                val = float(datadump[1])
                unit = next(a)
        
        if unit.upper()=="UGM":
            val = val
        elif unit.upper()=="PPB":
            return val*1.88
        elif unit.upper()=="PPM":
            return val*1.88/float(1000)
        
        logging.debug('Inser to Database: NO = %f' % val )
        datainsert('NO', dtn, val)
        
        # MQTT data send
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : val}
            
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NO'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
        
    elif out.find(b"CONC:") !=-1:
        datasplit = out.decode().split()
        NOX = float(datasplit[5])
        NO = float(datasplit[6])
        NO2 = float(datasplit[7])
        
        logging.debug('Inser to Database: NOX = %f' % NOX )
        datainsert('NOX', dtn, NOX)
        logging.debug('Inser to Database: NO = %f' % NO )
        datainsert('NO', dtn, NO)
        logging.debug('Inser to Database: NO2 = %f' % NO2 )
        datainsert('NO2', dtn, NO2)
        
        # MQTT data send
        if use_mqtt:
            payload = {'date' : dtn.isoformat(timespec='seconds'),'val' : NOX}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NOX'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
            
            payload = {'date' : dtn.isoformat(timespec='seconds'),'val' : NO}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NO'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
            
            payload = {'date' : dtn.isoformat(timespec='seconds'),'val' : NO2}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/NO2'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
    
    elif out.find(b'W ') == 0:
        logging.info(out)
        hasil = out.decode('utf-8').split()
        warn = ' '.join(hasil[3:])
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : warn}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/WT200'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
            
    else:
        logging.info(out)
        

if __name__ == '__main__':
    import signal
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    try:
        with serial.Serial(Port, Baudrate, timeout=Timeout,
                           parity = "N",stopbits = 1, bytesize = 8) as ser:
            while True:
                line = ser.readline()   # read a '\n' terminated line
                if line != b'':
                    procT200(line)
    except KeyboardInterrupt:
        client.disconnect() #disconnect
        client.loop_stop() #stop loop
        logging.warning('Exit Gracefully')