# -*- coding: utf-8 -*-
"""
Created on Mon May 20 14:47:10 2019

@author: luthf
"""

import configparser
import serial
import sys
import os
import datetime
import logging
import numpy as np
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
import pymongo

avetriger = False
dtn = datetime.datetime.utcnow()

levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

Port = config.get("AWS","Port")
Baudrate = config.getint("AWS","Baudrate")
Timeout = config.getint("AWS","Timeout")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=levels[loglevel])

# Inisialisasi MQTT
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))
    
try:
    logging.info('Trying to use mqtt')
    
    mqhost = config.get("MQTT","hostname")
    mqport = config.getint("MQTT","port")
    mquser = config.get("MQTT","username")
    mqpass = config.get("MQTT","password")
    mqtopic = config.get("MQTT","topic")
    mqloc  = config.get("Database","username")
    
    import paho.mqtt.client as mqtt
    import json
    
    client = mqtt.Client()
    client.username_pw_set(mquser, mqpass)
    client.on_connect = on_connect
    client.connect(mqhost, mqport, 60)
    client.loop_start()
    
    use_mqtt = True
except:
    logging.info('Warning! Failed to use MQTT')
    use_mqtt = False
# end of mqtt
    
def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
class awsminproc():
    def __init__(self):
        self.unitinit()
    
    def unitinit(self):
        self.dtn=datetime.datetime.utcnow()
        self.ws=np.array([])
        self.wd=np.array([])
        self.sp=np.array([])
        self.tp=np.array([])
        self.rh=np.array([])
        self.dp=np.array([])
        
    def vec2dir(self,u,v):
        wdir = np.arctan2(-u,-v) * 180/ np.pi
        try:
            for i in range(len(wdir)):
                if wdir[i] < 0:
                    wdir[i] = wdir[i] + 360
        except TypeError:
            if wdir < 0:
                wdir = wdir + 360
        wspd = np.sqrt(u**2 + v**2)
        return wspd,wdir
    
    def dir2vec(self,wspd,wdir):
        wdpi = wdir * np.pi / 180
        u = -wspd * np.sin(wdpi)
        v = -wspd * np.cos(wdpi)
        return u,v
        
    def unitaverage(self):
        # Wind
        u, v = self.dir2vec(self.ws,self.wd)
        u_ave = u.mean()
        v_ave = v.mean()
        ws_ave, wd_ave = self.vec2dir(u_ave, v_ave)
        
        retval = {
                  "WS_min":self.ws.min(),
                  "WS_max":self.ws.max(),
                  "WSpd":ws_ave, 
                  "WDir":wd_ave, 
                  "Press":self.sp.mean(),
                  "Temp":self.tp.mean(),
                  "DewPoint":self.dp.mean(),
                  "RH":self.rh.mean()}
        
        # clear data
        self.unitinit()
        
        return retval
    
    def parseAWS(self,out):
        if out.find(b"WIMMB") != -1:
            splist = out.decode().split(",")
            try:
                tempsp = float(splist[3])
                if tempsp < 2000:
                    self.sp = np.append(self.sp,tempsp)
            except:
                pass
                
        elif out.find(b"WIMTA") != -1:
            tplist = out.decode().split(",")
            try:
                temptp = float(tplist[1])
                if temptp < 100:
                    self.tp = np.append(self.tp,temptp)
            except:
                pass
            
        elif out.find(b"WIMHU") != -1:
            rhlist = out.decode().split(",")
            try:
                temprh = float(rhlist[1])
                if temprh <= 100:
                    self.rh = np.append(self.rh,temprh)
            except:
                pass
            try:
                tempdp = float(rhlist[3])
                if tempdp < 100:
                    self.dp = np.append(self.dp,tempdp)
            except:
                pass
            
        elif out.find(b"WIMWV") != -1:
            wwlist = out.decode().split(",")
            try:
                tempwd = float(wwlist[1])
                tempws = float(wwlist[3])
                if tempwd < 370 and tempws < 200:
                    self.wd = np.append(self.wd,tempwd)
                    self.ws = np.append(self.ws,tempws)
            except:
                pass
                
def datainsert(dtn,datadict):

    client = pymongo.MongoClient()
    db = client['dataraw']
    
    if 'AWS' not in db.list_collection_names():
        col = db['AWS']
        col.create_index([("date", pymongo.DESCENDING)],unique=True)
    else:
        col = db['AWS']
    logging.debug('Insert into database WS: %.1f WD: %.1f T: %.1f RH: %.1f' % (datadict['WSpd'], datadict['WDir'], datadict['Temp'],datadict['RH']))
    inid = col.update_one({'date': dtn},
                          {"$set": datadict},
                           upsert = True)
    
    return inid

#  Startting scheduler
def avetrg():
    global avetriger
    global dtn
    avetriger=True
    dtn = datetime.datetime.utcnow().replace(second=0,microsecond=0)
    
scheduler = BackgroundScheduler()
tgr = CronTrigger(minute='0-59/10')

job = scheduler.add_job(avetrg, trigger=tgr,
                  misfire_grace_time=20)

scheduler.start()
        
if __name__ == '__main__':
    import signal
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    aws = awsminproc()
    
    try:
        with serial.Serial(Port, Baudrate, timeout=Timeout,
                           parity = "N",stopbits = 1, bytesize = 8) as ser:
            while True:
                line = ser.readline()   # read a '\n' terminated line
                if line != b'':
                    aws.parseAWS(line)
                    
                if avetriger:
                    dataave = aws.unitaverage()
                    dataave['date'] = dtn
                    datainsert(dtn,dataave)
                    # MQTT data send
                    if use_mqtt:
                        payload = dataave
                        payload['date'] = dtn.isoformat(timespec='seconds')
                        payloads = json.dumps(payload)
                        topics = f'{mqtopic}/{mqloc}/AWS'
                        logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
                        client.publish(topics, payloads, qos = 2, retain=True)
                    avetriger = False
                    
    except KeyboardInterrupt:
        scheduler.shutdown()
        client.disconnect() #disconnect
        client.loop_stop() #stop loop
        logging.warning('Exit Gracefully')
