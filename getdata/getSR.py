# -*- coding: utf-8 -*-
"""
Created on Mon May 20 14:47:10 2019

@author: luthf
"""

import configparser
import serial
import sys
import os
import datetime
import logging
import numpy as np
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
import pymongo
import codecs

avetriger = False
dtn = datetime.datetime.utcnow()

levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

CodeTrigger = b"\x01\x03\x00\x00\x00\x01\x84\x0A"

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

Port = config.get("RAD","Port")
Baudrate = config.getint("RAD","Baudrate")
Timeout = config.getint("RAD","Timeout")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=levels[loglevel])

# Inisialisasi MQTT
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))
    
try:
    logging.info('Trying to use mqtt')
    
    mqhost = config.get("MQTT","hostname")
    mqport = config.getint("MQTT","port")
    mquser = config.get("MQTT","username")
    mqpass = config.get("MQTT","password")
    mqtopic = config.get("MQTT","topic")
    mqloc  = config.get("Database","username")
    
    import paho.mqtt.client as mqtt
    import json
    
    client = mqtt.Client()
    client.username_pw_set(mquser, mqpass)
    client.on_connect = on_connect
    client.connect(mqhost, mqport, 60)
    client.loop_start()
    
    use_mqtt = True
except:
    logging.info('Warning! Failed to use MQTT')
    use_mqtt = False
# end of mqtt
    
def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
class awsminproc():
    def __init__(self):
        self.unitinit()
    
    def unitinit(self):
        self.dtn=datetime.datetime.utcnow()
        self.sr=np.array([])
        
    def unitaverage(self):
        # SR
        retval = {"SR":self.sr.mean()}
        
        # clear data
        self.unitinit()
        
        return retval
    
    def parseSR(self,out):
        tempsr = float(out)
        self.sr = np.append(self.sr,tempsr)
        
                
def datainsert(dtn,datadict):

    client = pymongo.MongoClient()
    db = client['dataraw']
    
    if 'AWS' not in db.list_collection_names():
        col = db['AWS']
        col.create_index([("date", pymongo.DESCENDING)],unique=True)
    else:
        col = db['AWS']
    
    logging.debug('Insert to database: SR = %f' % datadict['SR'] )
    inid = col.update_one({'date': dtn},
                          {"$set": datadict},
                           upsert = True)
    
    return inid

#  Startting scheduler
def avetrg():
    global avetriger
    global dtn
    avetriger=True
    dtn = datetime.datetime.utcnow().replace(second=0,microsecond=0)
    
scheduler = BackgroundScheduler()
tgr = CronTrigger(minute='0-59/10')

job = scheduler.add_job(avetrg, trigger=tgr,
                  misfire_grace_time=20)

scheduler.start()
        
if __name__ == '__main__':
    import signal
    import time
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    aws = awsminproc()
    
    try:
        with serial.Serial(Port, Baudrate, timeout=0.2,
                           parity = "N",stopbits = 1, bytesize = 8) as ser:
            while True:
                ser.write(CodeTrigger)
                out = ser.read(size=7)
                try:
                    outs = out.hex()
                    if (outs[0:6] == '010302') & (len(outs) >=12):
                        SR = int(outs[6:10],16)
                        if SR < 2000:
                            checkSR = True
                        else:
                            checkSR = False
                    else:
                        checkSR = False
                except:
                    checkSR = False
                
                if checkSR:
                    aws.parseSR(SR)
                    
                    if avetriger:
                        dataave = aws.unitaverage()
                        dataave['date'] = dtn
                        datainsert(dtn,dataave)
                        # MQTT data send
                        if use_mqtt:
                            payload = dataave
                            payload['date'] = dtn.isoformat(timespec='seconds')
                            payloads = json.dumps(payload)
                            topics = f'{mqtopic}/{mqloc}/SR'
                            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
                            client.publish(topics, payloads, qos = 2, retain=True)
                        avetriger = False
                    
                time.sleep(1)
                    
    except KeyboardInterrupt:
        scheduler.shutdown()
        client.disconnect() #disconnect
        client.loop_stop() #stop loop
        logging.warning('Exit Gracefully')