# -*- coding: utf-8 -*-
"""
Created on Mon May 20 12:31:35 2019

@author: luthf
"""

import configparser
import serial
import sys
import os
from tools import datainsert
import datetime
import logging

levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

Port = config.get("T300","Port")
Baudrate = config.getint("T300","Baudrate")
Timeout = config.getint("T300","Timeout")
Dataunit = config.get("T300","Unit")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=levels[loglevel])

# Inisialisasi MQTT
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))
    
try:
    logging.info('Trying to use mqtt')
    
    mqhost = config.get("MQTT","hostname")
    mqport = config.getint("MQTT","port")
    mquser = config.get("MQTT","username")
    mqpass = config.get("MQTT","password")
    mqtopic = config.get("MQTT","topic")
    mqloc  = config.get("Database","username")
    
    import paho.mqtt.client as mqtt
    import json
    
    client = mqtt.Client()
    client.username_pw_set(mquser, mqpass)
    client.on_connect = on_connect
    client.connect(mqhost, mqport, 60)
    client.loop_start()
    
    use_mqtt = True
except:
    logging.info('Warning! Failed to use MQTT')
    use_mqtt = False
# end of mqtt
    
def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
def procT300(out):
    dtn = datetime.datetime.now(tz=datetime.timezone.utc)
    # Check for CONC1
    if out.find(b"CONC") !=-1:
        datasplit = out.decode().split()
        val = float(datasplit[-1])
        
        if Dataunit.upper()=="UGM":
            val = val
        elif Dataunit.upper()=="PPB":
            return val*1.145
        elif Dataunit.upper()=="PPM":
            return val*1.145/float(1000)
        
        logging.debug('Inser to Database: CO = %f' % val )
        datainsert('CO', dtn, val)
        
        # MQTT data send
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : val}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/CO'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
        
    elif out.find(b'W ') == 0:
        logging.info(out)
        hasil = out.decode('utf-8').split()
        warn = ' '.join(hasil[3:])
        if use_mqtt:
            payload = {
                    'date' : dtn.isoformat(timespec='seconds'),
                    'val' : warn}
            payloads = json.dumps(payload)
            topics = f'{mqtopic}/{mqloc}/WT300'
            logging.debug(f'Publishing MQTT with topic :{topics}, and payload :{payloads}')
            client.publish(topics, payloads, qos = 2, retain=True)
            
    else:
        logging.info(out)
        

if __name__ == '__main__':
    import signal
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    try:
        with serial.Serial(Port, Baudrate, timeout=Timeout,
                           parity = "N",stopbits = 1, bytesize = 8) as ser:
            while True:
                line = ser.readline()   # read a '\n' terminated line
                if line != b'':
                    procT300(line)
    except KeyboardInterrupt:
        client.disconnect() #disconnect
        client.loop_stop() #stop loop
        logging.warning('Exit Gracefully')