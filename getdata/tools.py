# -*- coding: utf-8 -*-
"""
Created on Mon May 20 05:54:23 2019

@author: luthf
"""

def datainsert(param,dtn,val):
    import pymongo
    
    client = pymongo.MongoClient()
    db = client['dataraw']
    
    if param not in db.list_collection_names():
        col = db[param]
        col.create_index([("date", pymongo.DESCENDING)],unique=True)
    else:
        col = db[param]
    
    inid = col.update_one({'date': dtn},
                          {"$set": {'date' : dtn,
                                    'value':val,
                                    }},
                           upsert = True)
    
    return inid

