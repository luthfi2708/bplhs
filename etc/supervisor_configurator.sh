#!/bin/bash
# Supervisord convigurator please run this script using sudo



if ! [ -x "$(command -v supervisorctl)" ]; then
    echo 'Error: supervisor is not installed.' >&2
    apt-get -y install supervisor
    tes=$(grep inet_http_server /etc/supervisor/supervisord.conf)
    if [[ $tes == "" ]]; then
        cat >> /etc/supervisor/supervisord.conf <<EOF

[inet_http_server]
port=*:9001
username=aqms
password=aqms
EOF

    fi
    
    systemctl enable supervisor.service
    systemctl restart supervisor.service
  
fi
    
mkdir -p /home/aqms/log
cp /home/aqms/bplhs/etc/supervisor_config/*.conf /etc/supervisor/conf.d/
supervisorctl reread
supervisorctl update 
