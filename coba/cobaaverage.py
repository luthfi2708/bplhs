# -*- coding: utf-8 -*-
"""
Created on Mon May 20 16:23:52 2019

@author: luthf
"""

import random
import time
import numpy as np
import datetime
from threading import Thread
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

val = np.array([])
avetriger = False
dtn = datetime.datetime.utcnow()

def avetrg():
    global avetriger
    global dtn
    avetriger=True
    dtn = datetime.datetime.utcnow()

scheduler = BackgroundScheduler()
tgr = CronTrigger(minute='0-59/10')

job = scheduler.add_job(avetrg, trigger=tgr,
                  misfire_grace_time=20)

scheduler.start()

try:
    while True:
        val = np.append(val, random.randint(0,10))
        if avetriger:
            print('average %s: %f' % (dtn.strftime('%Y-%m-%d %H:%M:%S'),val.mean()))
            val = np.array([])
            avetriger = False
        time.sleep(1)
        
except KeyboardInterrupt:
    scheduler.shutdown()
    print('Exit gracefully')
    