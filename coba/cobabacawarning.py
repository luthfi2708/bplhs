# -*- coding: utf-8 -*-
"""
Created on Mon May 20 10:25:10 2019

@author: luthf
"""
import sys
sys.path.append('../getdata')
import datetime

out = b'W  142:16:27  4001  SAMPLE FLOW WARN\r\n'

print(out)

# Check for CONC1
if out.find(b"CONC1=") !=-1 or out.find(b"CONC2=") != -1:
    datasplit = out.split()
    a = iter(datasplit)
    for linesplit in a:
        if linesplit.find("CONC1=") != -1 or linesplit.find("CONC2=") != -1:
            datadump = linesplit.split("=")
            val = float(datadump[1])
            unit = next(a)
    
        
    dtn = datetime.datetime.utcnow()
    
elif out.find(b'W ') == 0:
    hasil = out.decode('utf-8').split()
    warn = ' '.join(hasil[3:])
    print(warn)
else:
    print(b"Unprocessed data: " + out2)
    