# -*- coding: utf-8 -*-
"""
Created on Sun May 26 15:25:38 2019

@author: luthf
"""

import pymongo
import datetime
import pprint

client = pymongo.MongoClient('192.100.150.52',27017)
db = client['dataraw']

dtn = datetime.datetime(2019,5,26)
pipeline = [
        {'$match' : {'date': {'$gt':dtn}}},
        {'$group' : { 
                '_id' : 'average',
                'average' : { '$avg' : '$value'}}}]
tes = db['O3'].aggregate(pipeline)

hasil = list(tes)
if hasil:
    print('Average Value : %.2f' % hasil[0]['average'])