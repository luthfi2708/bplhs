# -*- coding: utf-8 -*-
"""
Created on Mon May 20 10:25:10 2019

@author: luthf
"""
import sys
sys.path.append('../getdata')
import datetime
from tools import valconvert

out2 = b'D  140:11:25  1001  CONC: AVG CONaC1=13.20 PPB\r\n'
out = out2.decode()
Dataunit = "UGM"

print(out)

# Check for CONC1
if out.find("CONC1=") !=-1 or out.find("CONC2=") != -1:
    datasplit = out.split()
    a = iter(datasplit)
    for linesplit in a:
        if linesplit.find("CONC1=") != -1 or linesplit.find("CONC2=") != -1:
            datadump = linesplit.split("=")
            val = float(datadump[1])
            unit = next(a)
    
    val = valconvert(val,unit,"UGM")
        
    dtn = datetime.datetime.utcnow()
    
else:
    print(b"Unprocessed data: " + out2)
    