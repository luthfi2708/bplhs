# -*- coding: utf-8 -*-
"""
Created on Sat May 18 11:21:21 2019

@author: luthf
"""
import serial
import signal

def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)

try:
    with serial.Serial('/dev/ttyS6', 9600, timeout=3,parity = "N",stopbits = 1, bytesize = 8) as ser:
        while True:
            line = ser.readline()   # read a '\n' terminated line
            if line != b'':
                print(line)
except KeyboardInterrupt:
    print('Exit Gracefully')
    raise KeyboardInterrupt