# -*- coding: utf-8 -*-
"""
Created on Mon May 20 11:31:37 2019

@author: luthf
"""

import logging

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)

logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')