# -*- coding: utf-8 -*-
"""
Created on Sat May 18 11:21:21 2019

@author: luthf
"""
import serial
import signal
import time

CodeTrigger = b"\x01\x03\x00\x00\x00\x01\x84\x0A"

def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)

try:
    with serial.Serial('/dev/ttyUSB1', 9600, timeout=0.2,parity = "N",stopbits = 1, bytesize = 8) as ser:
        while True:
            ser.write(CodeTrigger)
            out = ser.read(size=7)
            print(out)
            try:
                outs = out.hex()
                if (outs[0:6] == '010302') & (len(outs) >=12):
                    SR = int(outs[6:10],16)
                    if SR < 2000:
                        print(SR)
            except:
                pass
            time.sleep(1)
except KeyboardInterrupt:
    print('Exit Gracefully')
    raise KeyboardInterrupt
