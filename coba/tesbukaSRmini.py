# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 16:39:46 2019

@author: luthf
"""

CodeTrigger = b"\x01\x03\x00\x00\x00\x01\x84\x0A"

import serial
ser = serial.Serial('/dev/ttyUSB1', 9600, timeout=2,parity = "N",stopbits = 1, bytesize = 8)

ser.write(CodeTrigger)
out = ser.read(size=7)
print(out)
ser.close()

try:
    outs = out.hex()
    if (outs[0:6] == '010302') & (len(outs) >=12):
        SR = int(outs[6:10],16)
        print(SR)
except:
    pass
