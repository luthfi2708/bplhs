# -*- coding: utf-8 -*-
"""
Created on Sun May 26 12:41:57 2019

@author: luthf
"""

import time
import pymongo
import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
import json
import logging
import os, sys
import configparser
import requests

# Critical data
levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

ipserver = config.get("Database","hostname")
username = config.get("Database","username")
password = config.get("Database","password")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(message)s', level=levels[loglevel])

def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
def aveAnalizer(db, param, dtn, mindelta):
    pipeline = [
            {'$match' : {'date': {'$gt':dtn - datetime.timedelta(minutes=mindelta)}}},
            {'$group' : { 
                    '_id' : 'average',
                    'average' : { '$avg' : '$value'}}}]
    cur = db[param].aggregate(pipeline)
    
    hasil = list(cur)
    if hasil:
        return True, hasil[0]['average']
    else:
        return False, 0
    
    
def aveAWS(db,dtn):
    cur = db['AWS'].find({'date':dtn}) 
    
    hasil = list(cur)
    
    UV = Press = Temp = WSpd = WDir = RH = 0
    statUV = statPress = statTemp = statWSpd = statWDir = statRH = False
    if hasil:
        if 'SR' in hasil[0].keys():
            UV = hasil[0]['SR']
            statUV = True
        
        if 'Press' in hasil[0].keys():
            Press = hasil[0]['Press']
            statPress = True
            
        if 'Temp' in hasil[0].keys():
            Temp = hasil[0]['Temp']
            statTemp = True
            
        if 'WSpd' in hasil[0].keys():
            WSpd = hasil[0]['WSpd']
            statWSpd = True
            
        if 'WDir' in hasil[0].keys():
            WDir = hasil[0]['WDir']
            statWDir = True
            
        if 'RH' in hasil[0].keys():
            RH = hasil[0]['RH']
            statRH = True
    
    return UV, Press, Temp, WSpd, WDir, RH, statUV, statPress, statTemp, statWSpd, statWDir, statRH
    
#  Startting scheduler
def avesend(db):
    global avetriger
    global dtn
    global username
    avetriger=True
    dtn = datetime.datetime.utcnow().replace(second=0,microsecond=0)
    dtnsend = datetime.datetime.now().replace(second=0,microsecond=0)
    
    statCO, CO = aveAnalizer(db,'CO',dtn,10)
    statSO2, SO2 = aveAnalizer(db,'SO2',dtn,10)
    statNO, NO = aveAnalizer(db,'NO',dtn,10)
    statNO2, NO2 = aveAnalizer(db,'NO2',dtn,10)
    statO3, O3 = aveAnalizer(db,'O3',dtn,10)
    statPM10, PM10 = aveAnalizer(db,'PM10',dtn,120)
    statPM25, PM25 = aveAnalizer(db,'PM25',dtn,120)
    UV, Press, Temp, WSpd, WDir, RH, statUV, statPress, statTemp, statWSpd, statWDir, statRH = aveAWS(db,dtn)
    
    data1 = {"source": username, 
             "data": {
                     "CO":CO, 
                     "SO2":SO2, 
                     "NO":NO, 
                     "NO2":NO2, 
                     "PM10":PM10, 
                     "PM25":PM25, 
                     "O3":O3, 
                     "UV":UV, 
                     "Press":round(Press,2), 
                     "Temp":round(Temp,2),
                     "WSpd":round(WSpd,2),
                     "WDir":round(WDir),
                     "RH":round(RH)}, 
                 "timestamp":dtnsend.strftime('%Y-%m-%d %H:%M:%S') }
            
    data2 = {"source": username,
             "data": { 
                     "CO": True, 
                     "SO2": True, 
                     "NO": True, 
                     "NO2": True, 
                     "PM10": True, 
                     "PM25": True, 
                     "O3": True, 
                     "UV": True, 
                     "Press": True, 
                     "Temp": True, 
                     "WSpd": True, 
                     "WDir": True, 
                     "RH": True, 
                     "room_temp": 27, 
                     "alarm": False, 
                     "motion": False},
                 "timestamp": dtnsend.strftime('%Y-%m-%d %H:%M:%S')}
             
#    data2 = {"source": username,
#             "data": { 
#                     "CO": statCO, 
#                     "SO2": statSO2, 
#                     "NO": statNO, 
#                     "NO2": statNO2, 
#                     "PM10": statPM10, 
#                     "PM25": statPM25, 
#                     "O3": statO3, 
#                     "UV": statUV, 
#                     "Press": statPress, 
#                     "Temp": statTemp, 
#                     "WSpd": statWSpd, 
#                     "WDir": statWDir, 
#                     "RH": statRH, 
#                     "room_temp": 27, 
#                     "alarm": False, 
#                     "motion": False},
#                 "timestamp": dtnsend.strftime('%Y-%m-%d %H:%M:%S')}
            
    kirimdata(data1, data2)
    


def kirimdata(data1, data2, data2real):
    global ipserver, username, password
    url1 = "http://%s/api/sensors" % ipserver
    url2 = "http://%s/api/stations" % ipserver
    
    logging.info("Sending sensor payload to server: %s" % json.dumps(data1))
    response1 = requests.post(url1,
                              json = data1, 
                              auth = requests.auth.HTTPBasicAuth(username,password))
    
    logging.debug(response1.text)
    
    logging.info("Sending station payload to server: %s" % json.dumps(data2))
    response2 = requests.post(url2,
                              json = data2, 
                              auth = requests.auth.HTTPBasicAuth(username,password))
    logging.debug(response2.text)

if __name__ == '__main__' :
    import signal
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    scheduler = BackgroundScheduler()
    tgr = CronTrigger(minute='0-59/10',
                      second='1')
    client = pymongo.MongoClient()
    db = client['dataraw']
        
    job = scheduler.add_job(avesend, trigger=tgr,
                            args=[db],misfire_grace_time=20)
    
    scheduler.start()
    
    try:
        while True:
            time.sleep(0.1)
            
    except KeyboardInterrupt:
        logging.warning('Caught SIG KILL')
        
    finally:
        scheduler.shutdown()
        logging.warning('Exit Gracefully')