# -*- coding: utf-8 -*-
"""
Created on Mon May 27 06:14:16 2019

@author: luthf
"""

import pymongo
import logging
import datetime
import configparser
import requests
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
import os, sys
import json
import time

# Critical data
levels = {'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

config = configparser.ConfigParser()

config.read([os.path.join(sys.path[0],'../config/setting.ini'),
             os.path.join(os.path.expanduser("~"),'setting.ini')])

ipserver = config.get("Database","hostname")
username = config.get("Database","username")
password = config.get("Database","password")

loglevel = config.get("Logging","level")

logging.basicConfig(format='%(asctime)s - %(message)s', level=levels[loglevel])

def handler_stop_signals(signum, frame):
    raise KeyboardInterrupt
    
def aveAnalizer(db, param, dtn, mindelta):
    pipeline = [
            {'$match' : {'date': {'$gt':dtn - datetime.timedelta(minutes=mindelta)}}},
            {'$group' : { 
                    '_id' : 'average',
                    'average' : { '$avg' : '$value'}}}]
    cur = db[param].aggregate(pipeline)
    
    hasil = list(cur)
    if hasil:
        return hasil[0]['average']
    else:
        return 9999.9
    
def CalculateISPU_PM10(PM10Avg):
    if PM10Avg>=600:
        ISPU_PM10_Value = 500
    else:
        if PM10Avg>=500 and PM10Avg<600:
            PM10_lower = 500
            PM10_upper = 600
            ISP_lower = 400
            ISP_upper = 500
        elif PM10Avg>=420 and PM10Avg<500:
            PM10_lower = 420
            PM10_upper = 500
            ISP_lower = 300
            ISP_upper = 400
        elif PM10Avg>=350 and PM10Avg<420:
            PM10_lower = 350
            PM10_upper = 420
            ISP_lower = 200
            ISP_upper = 300
        elif PM10Avg>=150 and PM10Avg<350:
            PM10_lower = 150
            PM10_upper = 350
            ISP_lower = 100
            ISP_upper = 200
        elif PM10Avg>=50 and PM10Avg<150:
            PM10_lower = 50
            PM10_upper = 150
            ISP_lower = 50
            ISP_upper = 100
        else:
            PM10_lower = 0
            PM10_upper = 50
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(PM10_upper-PM10_lower)
        ISPU_PM10_Value = (A*(PM10Avg-PM10_lower))+ISP_lower
    ISPU_PM10_Value = float("%.0f" % ISPU_PM10_Value)
    return ISPU_PM10_Value

def CalculateISPU_O3(O3Avg):
    if O3Avg>=1200:
        ISPU_O3_Value = 500
    else:
        if O3Avg>=1000 and O3Avg<1200:
            O3_lower = 1000
            O3_upper = 1200
            ISP_lower = 400
            ISP_upper = 500
        elif O3Avg>=800 and O3Avg<1000:
            O3_lower = 800
            O3_upper = 1000
            ISP_lower = 300
            ISP_upper = 400
        elif O3Avg>=400 and O3Avg<800:
            O3_lower = 400
            O3_upper = 800
            ISP_lower = 200
            ISP_upper = 300
        elif O3Avg>=253 and O3Avg<400:
            O3_lower = 253
            O3_upper = 400
            ISP_lower = 100
            ISP_upper = 200
        elif O3Avg>=120 and O3Avg<253:
            O3_lower = 120
            O3_upper = 253
            ISP_lower = 50
            ISP_upper = 100
        else:
            O3_lower = 0
            O3_upper = 120
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(O3_upper-O3_lower)
        ISPU_O3_Value = (A*(O3Avg-O3_lower))+ISP_lower
    ISPU_O3_Value = float("%.0f" % ISPU_O3_Value)
    return ISPU_O3_Value

def CalculateISPU_CO(COAvg):
    if COAvg>=57.5:
        ISPU_CO_Value = 500
    else:
        if COAvg>=46 and COAvg<57.5:
            CO_lower = 46
            CO_upper = 57.5
            ISP_lower = 400
            ISP_upper = 500
        elif COAvg>=34 and COAvg<47.5:
            CO_lower = 34
            CO_upper = 57.5
            ISP_lower = 300
            ISP_upper = 400
        elif COAvg>=17 and COAvg<34:
            CO_lower = 17
            CO_upper = 34
            ISP_lower = 200
            ISP_upper = 300
        elif COAvg>=10 and COAvg<17:
            CO_lower = 10
            CO_upper = 17
            ISP_lower = 100
            ISP_upper = 200
        elif COAvg>=5 and COAvg<10:
            CO_lower = 5
            CO_upper = 10
            ISP_lower = 50
            ISP_upper = 100
        else:
            CO_lower = 0
            CO_upper = 5
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(CO_upper-CO_lower)
        ISPU_CO_Value = (A*(COAvg-CO_lower))+ISP_lower
    ISPU_CO_Value = float("%.0f" % ISPU_CO_Value)
    return ISPU_CO_Value

def CalculateISPU_NO2(NO2Avg):
    if NO2Avg>=1200:
        ISPU_NO2_Value = 500
    else:
        if NO2Avg>=1000 and NO2Avg<1200:
            NO2_lower = 1000
            NO2_upper = 1200
            ISP_lower = 400
            ISP_upper = 500
        elif NO2Avg>=800 and NO2Avg<1000:
            NO2_lower = 800
            NO2_upper = 1000
            ISP_lower = 300
            ISP_upper = 400
        elif NO2Avg>=400 and NO2Avg<800:
            NO2_lower = 400
            NO2_upper = 800
            ISP_lower = 200
            ISP_upper = 300
        elif NO2Avg>=253 and NO2Avg<400:
            NO2_lower = 253
            NO2_upper = 400
            ISP_lower = 100
            ISP_upper = 200
        elif NO2Avg>=120 and NO2Avg<253:
            NO2_lower = 120
            NO2_upper = 253
            ISP_lower = 50
            ISP_upper = 100
        else:
            NO2_lower = 0
            NO2_upper = 120
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(NO2_upper-NO2_lower)
        ISPU_NO2_Value = (A*(NO2Avg-NO2_lower))+ISP_lower
    ISPU_NO2_Value = float("%.0f" % ISPU_NO2_Value)
    return ISPU_NO2_Value

def CalculateISPU_PM25(PM25Avg):
    if PM25Avg>=1200:
        ISPU_PM25_Value = 500
    else:
        if PM25Avg>=1000 and PM25Avg<1200:
            PM25_lower = 1000
            PM25_upper = 1200
            ISP_lower = 400
            ISP_upper = 500
        elif PM25Avg>=800 and PM25Avg<1000:
            PM25_lower = 800
            PM25_upper = 1000
            ISP_lower = 300
            ISP_upper = 400
        elif PM25Avg>=400 and PM25Avg<800:
            PM25_lower = 400
            PM25_upper = 800
            ISP_lower = 200
            ISP_upper = 300
        elif PM25Avg>=253 and PM25Avg<400:
            PM25_lower = 253
            PM25_upper = 400
            ISP_lower = 100
            ISP_upper = 200
        elif PM25Avg>=120 and PM25Avg<253:
            PM25_lower = 120
            PM25_upper = 253
            ISP_lower = 50
            ISP_upper = 100
        else:
            PM25_lower = 0
            PM25_upper = 120
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(PM25_upper-PM25_lower)
        ISPU_PM25_Value = (A*(PM25Avg-PM25_lower))+ISP_lower
    ISPU_PM25_Value = float("%.0f" % ISPU_PM25_Value)
    return ISPU_PM25_Value


def CalculateISPU_NO(NOAvg):
    if NOAvg>=1200:
        ISPU_NO_Value = 500
    else:
        if NOAvg>=1000 and NOAvg<1200:
            NO_lower = 1000
            NO_upper = 1200
            ISP_lower = 400
            ISP_upper = 500
        elif NOAvg>=800 and NOAvg<1000:
            NO_lower = 800
            NO_upper = 1000
            ISP_lower = 300
            ISP_upper = 400
        elif NOAvg>=400 and NOAvg<800:
            NO_lower = 400
            NO_upper = 800
            ISP_lower = 200
            ISP_upper = 300
        elif NOAvg>=253 and NOAvg<400:
            NO_lower = 253
            NO_upper = 400
            ISP_lower = 100
            ISP_upper = 200
        elif NOAvg>=120 and NOAvg<253:
            NO_lower = 120
            NO_upper = 253
            ISP_lower = 50
            ISP_upper = 100
        else:
            NO_lower = 0
            NO_upper = 120
            ISP_lower = 0
            ISP_upper = 50
        A = (ISP_upper-ISP_lower)/float(NO_upper-NO_lower)
        ISPU_NO_Value = (A*(NOAvg-NO_lower))+ISP_lower
    ISPU_NO_Value = float("%.0f" % ISPU_NO_Value)
    return ISPU_NO_Value

def CalculateISPU_SO2(SO2Avg):
    if SO2Avg>=2620:
        ISPU_S02_Value = 500
    else:
        if SO2Avg>=2100 and SO2Avg<2620:
            SO2_lower = 2100
            SO2_upper = 2620
            ISP_lower = 400
            ISP_upper = 500
        elif SO2Avg>=1600 and SO2Avg<2100:
            SO2_lower = 1600
            SO2_upper = 2100
            ISP_lower = 300
            ISP_upper = 400
        elif SO2Avg>=800 and SO2Avg<1600:
            SO2_lower = 800
            SO2_upper = 1600
            ISP_lower = 200
            ISP_upper = 300
        elif SO2Avg>=365 and SO2Avg<800:
            SO2_lower = 365
            SO2_upper = 800
            ISP_lower = 100
            ISP_upper = 200
        elif SO2Avg>=80 and SO2Avg<365:
            SO2_lower = 80
            SO2_upper = 365
            ISP_lower = 50
            ISP_upper = 100
        else:
            SO2_lower = 0
            SO2_upper = 80
            ISP_lower = 0
            ISP_upper = 50

        A = (ISP_upper-ISP_lower)/float(SO2_upper-SO2_lower)
        ISPU_S02_Value = (A*(SO2Avg-SO2_lower))+ISP_lower
    ISPU_S02_Value = float("%.0f" % ISPU_S02_Value)
    return ISPU_S02_Value

#  Startting scheduler
def ispusend(db):
    global avetriger
    global dtn
    avetriger=True
    dtn = datetime.datetime.utcnow().replace(second=0,microsecond=0)
    dtnsend = datetime.datetime.now().replace(second=0,microsecond=0)
    
    data = {"source": username, 
             "data": {
                     "CO":CalculateISPU_CO(aveAnalizer(db,'PM10',dtn,480)), 
                     "SO2":CalculateISPU_SO2(aveAnalizer(db,'PM10',dtn,1440)), 
                     "NO":CalculateISPU_NO(aveAnalizer(db,'NO',dtn,60)), 
                     "NO2":CalculateISPU_NO2(aveAnalizer(db,'NO2',dtn,60)), 
                     "PM10":CalculateISPU_PM10(aveAnalizer(db,'PM10',dtn,1500)), 
                     "PM25":CalculateISPU_PM25(aveAnalizer(db,'PM25',dtn,1500)), 
                     "O3":CalculateISPU_O3(aveAnalizer(db,'PM10',dtn,60))}, 
                 "timestamp":dtnsend.strftime('%Y-%m-%d %H:%M:%S') }
        
    kirimdata(data)
    
def kirimdata(data1):
    global ipserver, username, password
    url = "http://%s/api/sensors/ispu" % ipserver
    
    logging.info("Sending sensor payload to server: %s" % json.dumps(data1))
    response = requests.post(url,
                              json = data1, 
                              auth = requests.auth.HTTPBasicAuth(username,password))
    
    logging.debug(response.text)
    
if __name__ == '__main__':
    import signal
    
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    
    scheduler = BackgroundScheduler()
    tgr = CronTrigger(minute='0-59/10',
                      second='1')
    client = pymongo.MongoClient()
    db = client['dataraw']
        
    job = scheduler.add_job(ispusend, trigger=tgr,
                            args=[db],misfire_grace_time=20)
    
    scheduler.start()
    
    dtn = datetime.datetime.utcnow().replace(second=0,microsecond=0)
    
    try:
        while True:
            time.sleep(0.1)
            
    except KeyboardInterrupt:
        logging.warning('Caught SIG KILL')
        
    finally:
        scheduler.shutdown()
        logging.warning('Exit Gracefully')